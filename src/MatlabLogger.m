classdef MatlabLogger < handle
    %MatlabLogger   Logger for Matlab workers in the MIA framework.
    %
    %   This is a simple logger for Matlab workers in the Medical Image 
    %   Analysis (MIA) framework. The logger offers three different logging
    %   functionalities:
    %      1) Logging to the Matlab command window (enabled by default).
    %      2) Logging to log file (disabled by default)
    %      3) Logging to the MIA containerservice via REST calls (disabled by
    %         default)
    %
    %   MatlabLogger uses the same log level hierarcy as log4j and log4m:
    %   {'ALL','TRACE','DEBUG','INFO','WARN','ERROR','FATAL','OFF'}.
    %
    %   Parts of the implementation of MatlabLogger are based on log4m:
    %   https://www.mathworks.com/matlabcentral/fileexchange/37701-log4m-a-powerful-and-simple-logger-for-matlab

    properties (Constant)
        LEVEL_ALL = 0;
        LEVEL_TRACE = 1;
        LEVEL_DEBUG = 2;
        LEVEL_INFO = 3;
        LEVEL_WARN = 4;
        LEVEL_ERROR = 5;
        LEVEL_FATAL = 6;
        LEVEL_OFF = 7;
        LEVEL_NAMES = {'ALL', 'TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL', 'OFF'};
                
        STATUS_IDLE = 0;
        STATUS_PREPROCESSING = 1;
        STATUS_QUEUE = 2;
        STATUS_RUNNING = 3;
        STATUS_DONE = 4;
        STATUS_CONFIGURATIONERROR = 5;
        STATUS_MAPPINGERROR = 6;
        STATUS_VALIDATIONERROR = 7;
        STATUS_CALCULATIONERROR = 8;
        STATUS_EXPORTERROR = 9;
        STATUS_CONNECTIONERROR = 10;
        STATUS_NAMES = {'IDLE', 'PREPROCESSING', 'QUEUE', 'RUNNING', 'DONE', 'CONFIGURATIONERROR', 'MAPPINGERROR', 'VALIDATIONERROR', 'CALCULATIONERROR', 'EXPORTERROR', 'CONNECTIONERROR'};
    end
    
    properties (Access = private)
        cmdLogLevel = MatlabLogger.LEVEL_DEBUG; % Log level for logging to the Matlab command window (set to 'DEBUG' by default)
        fileLogLevel = MatlabLogger.LEVEL_OFF;  % Log level for logging to file (set to 'OFF' by default)
        restLogLevel = MatlabLogger.LEVEL_OFF;  % Log level for logging to a REST service (set to 'OFF' by default)
        logFile = '';       % File path of log file
        restURL = '';       % URL of RESTful service
        serviceName = '';   % Name of MIA microservice within which the logger is being used
        containerId = -1;   % ID of the container that is being processed
        doseFraction = -1;      % Dose fraction that is being processed
        ctFraction = -1;    % CT fraction that is being processed
        
        logMemory = true;
        availableMemoryMSG = '';
    end
    
    %% Public static methods
    methods (Access = public, Static=true)
        function this = getLogger()
            %getLogger   Returns instance unique logger object.
            %
            %   obj = MatlabLogger.getLogger() returns an instance unique logger 
            %   object.
            %
            %   Output
            %   ------
            %      this : MatlabLogger handle
            %         Reference to instance unique MatlabLogger object.
            persistent localObj;
            if isempty(localObj) || ~isvalid(localObj)
                localObj = MatlabLogger();
            end
            this = localObj;
        end
    end
    
    %% Public non-static methods
    methods (Access = public, Static = false)
        
        %% Public configuration methods 
        function status = enableCommandWindowLogging(this, logLevel)
            %enableCommandWindowLogging   Enables logging to the Matlab command window. 
            %
            %   Enabled by default with DEBUG level. Returns 1 if successful and 0 
            %   if failed.
            %
            %   Input
            %   -----
            %      logLevel : Integer
            %         Log level, integer between 0 and 7.
            %
            %   Output
            %   ------
            %      status: Integer
            %         Status (1 = success, 0 = failure).
            this.setCommandWindowLogLevel(logLevel);
            status = 1;
        end
        
        function status = enableFileLogging(this, logLevel, logFile)
            %enableFileLogging   Enables logging to file.
            %
            %   Disabled by default. Returns 1 if successful and 0 if failed.
            %
            %   Input
            %   -----
            %      logLevel : Integer
            %         Log level, integer between 0 and 7.
            %      logFile : String
            %         File path of the log file to which the log is to be written.
            %
            %   Output
            %   ------
            %      status: Integer
            %         Status (1 = success, 0 = failure).
            try
                fid = fopen(logFile, 'a');
                fclose(fid);
            catch ME
                this.warn(this.STATUS_RUNNING, 'cfff84bd-ab53-43c9-853f-1a6b6afbefff', ['Unable to open log file ''' logFile '''.''']);
                this.warn(this.STATUS_RUNNING, 'f0d02f09-28ac-4a15-a018-5cdea4e34733', 'Logging to file will be disabled.');
                status = 0;
                return;
            end
            this.setFileLogLevel(logLevel);
            this.logFile = logFile;
            status = 1;
        end
        
        function status = enableRestLogging(this, logLevel, restURL)
            %enableRestLogging   Enables logging to a RESTful service.
            %
            %   Disabled by default. Returns 1 if successful and 0 if failed.
            %
            %   Input parameters
            %   ----------------
            %      logLevel : Integer
            %         Log level, integer between 0 and 7.
            %      restURL : String
            %         Address of the RESTful service.
            %
            %   Output
            %   ------
            %      status: Integer
            %         Status (1 = success, 0 = failure).
			try
				[content, status] = urlread(restURL, 'post', {});
			catch ME
				status = 0;
			end
			if status == 1
                this.setRestLogLevel(logLevel);
                this.restURL = restURL;
            else
                this.warn(this.STATUS_RUNNING, '7735ea5b-6eb3-4601-865e-16e16c3710c1', ['Unable to reach RESTful service at URL ''' restURL '''.']);
                this.warn(this.STATUS_RUNNING, '4bb2dcc0-36d8-4fee-b601-61ebd49b64b7', 'Logging to RESTful service will be disabled.');
            end
        end
        
        function disableCommandWindowLogging(this)
            %disableRestLogging   Disables logging to the Matlab command window.
            this.setCommandWindowLogLevel(this.LEVEL_OFF);
        end
        
        function disableFileLogging(this)
            %disableFileLogging   Disables logging to file.
            this.setFileLogLevel(this.LEVEL_OFF);
            this.logFile = '';
        end
        
        function disableRestLogging(this)
            %disableRestLogging   Disables logging to a RESTful service.
            this.setRestLogLevel(this.LEVEL_OFF);
            this.restURL = '';
        end
        
        function bool = isCommandWindowLoggingEnabled()
            %isCommandWindowLoggingEnabled
            %   Returns 'true' if logging to command window is enabled and 'false' 
            %   otherwise.
            bool = (this.cmdLogLevel < 7);
        end
        
        function bool = isFileLoggingEnabled()
            %isFileLoggingEnabled   
            %   Returns 'true' if logging to file is enabled and 'false' otherwise.
            bool = (this.fileLogLevel < 7);
        end
        
        function bool = isRestLoggingEnabled()
            %isRestLoggingEnabled
            %   Returns 'true' if logging to a RESTful service is enabled and 'false' 
            %   otherwise.
            bool = (this.restLogLevel < 7);
        end
        
        function setCommandWindowLogLevel(this, logLevel)
            this.cmdLogLevel = logLevel;
        end
        
        function setFileLogLevel(this, logLevel)
            this.fileLogLevel = logLevel;
        end
        
        function setRestLogLevel(this, logLevel)
            this.restLogLevel = logLevel;
        end
        
        function setServiceName(this, serviceName)
            this.serviceName = serviceName;
        end
        
        function setContainerId(this, containerId)
            this.containerId = containerId;
        end
        
        function setDoseFraction(this, doseFraction)
            this.doseFraction = doseFraction;
        end

        function setCtFraction(this, ctFraction)
            this.ctFraction = ctFraction;
        end
        
        function setMemoryLogging(this, bool)
            this.logMemory = bool;
        end
        
        function bool = getMemoryLogging(this)
            bool = this.logMemory;
        end
            
        
        %% Public logging methods
        function trace(this, status, uuid, msg)
            %trace   Log a message with the TRACE level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_TRACE, status, uuid, msg);
        end
        
        function debug(this, status, uuid, msg)
            %debug   Log a message with the DEBUG level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_DEBUG, status, uuid, msg);
        end
        
        function info(this, status, uuid, msg)
            %info   Log a message with the INFO level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_INFO, status, uuid, msg);
        end
        
        function warn(this, status, uuid, msg)
            %warn   Log a message with the WARN level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_WARN, status, uuid, msg);
        end
        
        function error(this, status, uuid, msg)
            %error   Log a message with the ERROR level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_ERROR, status, uuid, msg);
        end
        
        function fatal(this, status, uuid, msg)
            %fatal   Log a message with the FATAL level.
            %
            %   Input parameters
            %   ----------------
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            this.log(this.LEVEL_FATAL, status, uuid, msg);
        end
        
        function log(this, level, status, uuid, msg)
            %log   Log a message with the specified level.
            %
            %   Input parameters
            %   ----------------
            %      level : Integer
            %         Log level, integer between 0 and 7.
            %      status : String
            %         Service status (e.g. RUNNING, CALCULATIONERROR, etc.).
            %      uuid : String
            %         Unique message identifier.
            %      msg : String
            %         Message.
            date = datestr(now, 'yyyy-mm-dd HH:MM:SS.FFF');
            this.evaluateAvailableMemory();
            this.writeLogToCmd(date, level, status, uuid, msg);
            this.writeLogToFile(date, level, status, uuid, msg);
            this.writeLogToRest(date, level, status, uuid, msg);
        end
        
    end
    
    %% Private non-static methods
    methods (Access = private, Static = false)
        
        %% Constructor
        function this = MatlabLogger()
            % empty constructor
        end
        
        %% Private logging methods
        function writeLogToCmd(this, date, level, status, uuid, msg)
            
            % Skip if message level is below log level
            if (level < this.cmdLogLevel)
                return;
            end
                        
            levelName = this.LEVEL_NAMES{level + 1};
            statusName = this.STATUS_NAMES{status + 1};
            
            if this.logMemory
                fprintf('%s  %s  %s %i %s \t : %s\n', date, levelName, this.serviceName, this.containerId, this.availableMemoryMSG, msg);
            else
                fprintf('%s  %s  %s %i \t : %s\n', date, levelName, this.serviceName, this.containerId, msg);
            end
            
        end
        
        function writeLogToFile(this, date, level, status, uuid, msg)
            
            % Skip if message level is below log level
            if (level < this.fileLogLevel)
                return;
            end
            
            levelName = this.LEVEL_NAMES{level + 1};
            statusName = this.STATUS_NAMES{status + 1};
            
            try
                fid = fopen(this.logFile, 'a');
                if this.logMemory
                    fprintf(fid, '%s  %s  %s %i %s \t : %s\n', date, levelName, this.serviceName, this.containerId, this.availableMemoryMSG, msg);
                else
                    fprintf(fid, '%s  %s  %s %i \t : %s\n', date, levelName, this.serviceName, this.containerId, msg);
                end
                fclose(fid);
            catch ME
                this.writeLogToCmd(date, this.LEVEL_WARN, this.STATUS_RUNNING, '678897ff-8cf5-4baf-87dc-a71bdaae3e40', ['Unable to log to file at path ''' this.logFile '''.']);
                this.writeLogToCmd(date, this.LEVEL_WARN, this.STATUS_RUNNING, 'a58edfde-f65b-4035-8f06-b682b0ac0acc', [ME.identifier ': ' ME.message]);
            end
            
        end
        
        function writeLogToRest(this, date, level, status, uuid, msg)
            
            % Skip log if message level is below log level
            if(level < this.restLogLevel)
                return;
            end 
            
            levelName = this.LEVEL_NAMES{level + 1};
            statusName = this.STATUS_NAMES{status + 1};
            
            msgStruct = struct();
            msgStruct.entryDate = date;
            msgStruct.level = levelName;
            msgStruct.containerId = this.containerId;
            msgStruct.doseFraction = num2str(this.doseFraction);
            msgStruct.ctFraction = num2str(this.ctFraction);
            msgStruct.serviceName = this.serviceName;
            msgStruct.status = statusName;
            msgStruct.messageUuid = num2str(uuid);
            msgStruct.message = msg;
            
            try
                options = weboptions('MediaType', 'application/json', 'ContentType', 'json', 'Timeout', 5);
                response = webwrite(this.restURL, msgStruct, options);
            catch ME
                this.writeLogToCmd(date, this.LEVEL_WARN, this.STATUS_RUNNING, 'd7fa88e9-e70a-4254-89ad-540f88f68339', ['Unable to log to RESTful service with URL ''' this.restURL '''.']);
                this.writeLogToCmd(date, this.LEVEL_WARN, this.STATUS_RUNNING, 'bb8e3ea6-ab96-42d9-b692-538d44ac0558', [ME.identifier ': ' ME.message]);
                this.writeLogToFile(date, this.LEVEL_WARN, this.STATUS_RUNNING, 'd7fa88e9-e70a-4254-89ad-540f88f68339', ['Unable to log to RESTful service with URL ''' this.restURL '''.']);
                this.writeLogToFile(date, this.LEVEL_WARN, this.STATUS_RUNNING, 'bb8e3ea6-ab96-42d9-b692-538d44ac0558', [ME.identifier ': ' ME.message]);
            end
            
        end
        
        function evaluateAvailableMemory(this)
            if this.logMemory
                mem = memory;
                maxArraySizeBytes = mem.MaxPossibleArrayBytes;
                maxArraySizeMegaBytes = maxArraySizeBytes / 1024 / 1024;
                this.availableMemoryMSG = [num2str(round(maxArraySizeMegaBytes)), 'MB'];
            end
        end
    end
    
end