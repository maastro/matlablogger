classdef MatlabLoggerTest < matlab.unittest.TestCase
    %MatlabLoggerTest   Tests the MatlabLogger class.
    
    properties
        NUMLOGS = 1e3;
        filePath = [pwd filesep 'test.log'];
        restURL = 'http://localhost:8210/api/functionallog/';
        containerId = 1;
        doseFraction = 1;
        ctFraction = 10;
        logger;
    end
    
    methods (TestClassSetup)
        function setupOnce(this)
            this.logger = MatlabLogger.getLogger();
            this.logger.setServiceName('Test');
            this.logger.setContainerId(this.containerId);
            this.logger.setDoseFraction(this.doseFraction);
            this.logger.setCtFraction(this.ctFraction);
        end
    end
    
    methods (Test)
        function testLoggingOff(this)
            %testLoggingOff   Tests with logging off.
            %   Write NUMLOGS logs with all logging off.
            this.logger.disableCommandWindowLogging();
            this.logger.disableFileLogging();
            this.logger.disableRestLogging();
            for i = 1:this.NUMLOGS
                this.logger.info(this.logger.STATUS_IDLE, ['1-' num2str(i)], ['Test 1 - Log ' num2str(i) ' / ' num2str(this.NUMLOGS) ]);
            end
        end
        
        function testCmdLogging(this)
            %testCmdLogging   Tests command window logging.
            %    Write NUMLOGS logs to the Matlab command window.
            this.logger.disableFileLogging();
            this.logger.disableRestLogging();
            status = this.logger.enableCommandWindowLogging(this.logger.LEVEL_TRACE);
            this.verifyEqual(status, 1);
            for i = 1:this.NUMLOGS
                this.logger.info(this.logger.STATUS_IDLE, ['2-' num2str(i)], ['Test 2 - Log ' num2str(i) ' / ' num2str(this.NUMLOGS) ]);
            end
        end
        
        function testFileLogging(this)
            %testFileLogging   Tests file logging.
            %    Write NUMLOGS logs to file.
            this.logger.disableCommandWindowLogging();
            this.logger.disableRestLogging();
            status = this.logger.enableFileLogging(this.logger.LEVEL_TRACE, this.filePath);
            this.verifyEqual(status, 1);
            if status == 1
                for i = 1:this.NUMLOGS
                    this.logger.info(this.logger.STATUS_IDLE, ['3-' num2str(i)], ['Test 3 - Log ' num2str(i) ' / ' num2str(this.NUMLOGS) ]);
                end
            end
            delete(this.filePath);
        end
           
        function testRestLogging(this)
            %testFileLogging   Tests REST logging to MIA containerservice.
            %    Write NUMLOGS logs to the MIA containerservice via REST calls.
            %
            %    Successful completion of this test requires that the MIA 
            %    containerservice is up and running at {this.restURL}, and that there
            %    exists a container with ID {this.containerId}.
            this.logger.disableCommandWindowLogging();
            this.logger.disableFileLogging();
            status = this.logger.enableRestLogging(this.logger.LEVEL_TRACE, this.restURL);
            this.verifyEqual(status, 1);
            if status == 1
                for i = 1:this.NUMLOGS
                    this.logger.info(this.logger.STATUS_IDLE, ['4-' num2str(i)], ['Test 4 - Log ' num2str(i) ' / ' num2str(this.NUMLOGS) ]);
                end
            end
        end
    end
end   